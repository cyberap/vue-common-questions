# Ответы на типичные вопросы во vuejs_club

## Почему у меня не работают картинки с динамическими путями?

Обычно такой код выглядит примерно вот так:

```html
<template>
  <img :src="`./img/${dynamicPath}.png`">
</template>
```

Чаще всего до изменений код выглядел следующим образом и «всё работало»:

```html
<template>
  <img src="./img/staticPath.png">
</template>
```

Так происходит потому что [vue-loader преобразует такие пути во время сборки проекта](https://vue-loader.vuejs.org/ru/guide/asset-url.html#%D0%BE%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-%D0%B2n%D0%BE%D0%B6%D0%B5%D0%BD%D0%BD%D1%8B%D1%85-url).

Но это работает только для статических путей. Для динамических путей нужно явно импортировать такие картинки:

```vue
<template>
  <img :src="selectedImage">
</template>

<script>
  import image1 from './img/image1.png';
  import image2 from './img/image2.png';
  import image3 from './img/image3.png';
  
  const imagePaths = [
    image1,
    image2,
    image3,
  ];
  
  export default {
    data() {
      return {
        selectedImageId: 0,
      };
    },
    computed: {
      selectedImage() {
        return imagePaths[this.selectedImageId];
      },
    },            
  }
</script>
```


## Как мне использовать v-model с пропсами?

`v-model` нельзя просто так использовать с пропсами, всего лишь указав пропс как значение `v-model`.

Если вы попробуете так сделать то получите ошибку что вы пытаетесь мутировать пропс, что запрещено делать во вью.

Чтобы решить эту проблему вам нужно использовать геттер и сеттер для `v-model`:

```vue
<template>
  <input v-model="inputModel">
</template>

<script>
  export default {
    // Добавляем конфиг 'model' чтобы быть совместимыми с Vue 3
    // Можете игнорировать если у вас уже Vue 3
    model: {
      prop: 'modelValue',
      event: 'update:modelValue',
    },
    props: {
      modelValue: {
        type: String,
      },
    },
    computed: {
      inputModel: {
        get() {
          return this.modelValue;
        },
        set(value) {
          this.$emit('update:modelValue', value);
        },
      }
    },
  }
</script>
```

## Чем мне заменить миксины?

Использование миксинов во Vue является нежелательной практикой. От миксинов [отказались в Реакте](https://ru.reactjs.org/blog/2016/07/13/mixins-considered-harmful.html) и [перестали их рекомендовать во Vue 3](https://vuejs.org/api/options-composition.html#mixins). Vue предлагает несколько вариантов переиспользования логики.

### Функции

Если в вашей логике нет реактивности то будет достаточно вынести её в функцию и импортировать по месту использования.

#### До

```js
export const prependPlusSignMixin = {
  methods: {
    prependPlusSign(number) {
      return `+${number.replace(/^\+/, '')}`;
    },
  },
};
```

#### После

```js
// prepend-plus-sign.js
export function prependPlusSign(number) {
  return `+${number.replace(/^\+/, '')}`;
}
```

```js
// component.js
import { prependPlusSign } from './prepend-plus-sign.js';

export default {
  methods: {
    prependPlusSign,
  },
};
```

### Composition API

Если в вашем миксине есть реактивность то эту логику лучше переписать на использование [Composition API](https://v3.vuejs.org/guide/composition-api-introduction.html).
В итоге вы получите так называемый `composable` — функцию-фабрику, которая использует Composition API.

#### До

```js
export const counterMixin = {
  data() {
    return { counter: 0 };
  },
  methods: {
    increment() {
      this.counter++;
    },
  },
  computed: {
    doubledCounter() {
      return this.counter * 2;
    },
  },
};
```

#### После

```js
// counter-composable.js
import { ref, computed } from 'vue';

export const useCounter = (initialValue = 0) => {
  const counter = ref(initialValue);
  const increment = () => counter.value++;
  const doubledCounter = computed(() => counter.value * 2);
  return { increment, doubledCounter };
};
```

```js
import { useCounter } from './counter-composable.js';

export default() {
  setup() {
    const { increment, doubledCounter } = useCounter();
    return { increment, doubledCounter };
  },
};
```

### Компоненты

Если у вас нет возможности использовать Composition API (например у вас Vue 2 и вы не можете установить плагин [`composition-api`](https://github.com/vuejs/composition-api)), то вы можете использовать компоненты для переиспользования реактивной логики.


#### До

```js
export const counterMixin = {
  data() {
    return { counter: 0 };
  },
  methods: {
    increment() {
      this.counter++;
    },
  },
  computed: {
    doubledCounter() {
      return this.counter * 2;
    },
  },
};
```

#### После

```js
// doubled-counter.js
export default {
  name: 'DoubledCounter',
  props: {
    initialValue: {
      type: Number,
      default: 0,
    },
  },
  data() {
    return {
      counter: this.initialValue,
    };
  },
  methods: {
    increment() {
      this.counter++;
    },
  },
  computed: {
    doubledCounter() {
      return this.counter * 2;
    },
  },
  template: `<div><slot :doubledCounter="doubledCounter" :increment="increment" /><div>`
};
```

```js
import DoubledCounter from 'doubled-counter.js';

export default {
  components: {
    DoubledCounter,
  },
  template: `
    <div>
      <DoubledCounter>
        <template #default="{ doubledCounter, increment }">
          <div>Counter: {{ doubledCounter }}</div>
          <button @click="increment">Increment</button>
        </template>
      </DoubledCounter>
    </div>
  `,
};
```
